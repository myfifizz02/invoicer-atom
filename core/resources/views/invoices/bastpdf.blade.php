<div class="container">
    <div style="width:150px;height:150px;float:left;">
        @if($invoice->pdf_logo != '')
            <img src="{{ $invoice->pdf_logo }}" alt="logo" width="80%"/>
        @endif
    </div>
    <div style="width:300px;height:150px;float:left;">
        @if($settings)
            <h1 style="margin: 0"><strong>{{ $settings->name }}</strong></h1>
            {{ $settings->address1 ? $settings->address1.',' : '' }} {{ $settings->state ? $settings->state : '' }}<br/>
            {{ $settings->city ? $settings->city.',' : '' }} {{ $settings->postal_code ? $settings->postal_code.','  : ''  }}<br/>
            {{ $settings->country }}<br/>
            @if($settings->phone != '')
                {{trans('application.phone')}}: {{ $settings->phone }}<br/>
            @endif
            @if($settings->email != '')
                {{trans('application.email')}}: {{ $settings->email }}.
            @endif
        @endif
    </div>
    <div style="clear: both"></div>
    <div class="text-right" style="width:300px;float:right;">
        <div class="text-right"> <h2 style="margin: 0;font-weight: bold;">{{trans('application.bast_desc')}}</h2></div>
    </div>
    <div style="clear: both"></div>
    <div class="panel-body" style="font-size: 16px;font-weight: bold;padding: 0; margin-top: 25px">
        <table style="width: 100%">
            <tr>
                <td class="text-left" style="width: 100px">{{trans('application.date')}}</td>
                <td style="width: 20px">:</td>
                <td class="text-left" style="color: red">{{ format_date($invoice->date) }}</td>
            </tr>
            <tr>
                <td class="text-left">{{trans('application.reference')}}</td>
                <td>:</td>
                <td class="text-left" style="color: red"> {{ $invoice->number }}</td>
            </tr>
        </table>
    </div>
    <div class="col-md-12">
        <div style="color: red;font-weight: bold;margin: 25px 0">
            <div style="color: red;font-weight: bold">{{trans('application.customer')}} </div>
            {{ $invoice->client->name }}<br>
            {{ $invoice->client->address1 ? $invoice->client->address1.',' : '' }} {{ $invoice->client->state ? $invoice->client->state : '' }}<br/>
            {{ $invoice->client->city ? $invoice->client->city.',' : '' }} {{ $invoice->client->postal_code ? $invoice->client->postal_code.','  : ''  }}<br/>
            {{ $invoice->client->country }}<br/>
            @if($invoice->client->phone != '')
                {{trans('application.phone')}}: {{ $invoice->client->phone }}<br/>
            @endif
            @if($invoice->client->email != '')
                {{trans('application.email')}}: {{ $invoice->client->email }}<br>
            @endif
            @if($invoice->client->npwp != '')
                {{trans('application.tax_number')}}: {{ $invoice->client->npwp }}
            @endif
        </div>
    </div>
    <div style="clear: both"></div>
    <div class="col-md-12">
        <table class="table">
            <thead style="margin-bottom:5px;font-weight: bold;color: black">
            <tr style="margin-bottom:5px;font-weight: bold;color: black;text-align: left;">
                <th align="left" style="width: 60%;"><span style="margin-left: 10px">{{trans('application.product')}}</span></th>
                <th class="text-center" style="width: 25%">{{trans('application.unit')}}</th>
                <th class="text-center" style="width: 15%">{{trans('application.quantity')}}</th>
            </tr>
            </thead>
            <tbody id="items" style="background-color: white;color: red">
            @foreach($invoice->items->sortBy('item_order') as $item)
            <tr>
                <td align="left"><b>{{ $item->item_name }}</b><br/>{!! htmlspecialchars_decode(nl2br(e($item->item_description)),ENT_QUOTES) !!}</td>
                <td class="text-center"><b>{{ $item->unit }}<b></td>
                <td class="text-center"><b>{{ $item->quantity }}<b></td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-md-12">
        <table style="width: 100%;margin-top: 50px">
            <tr>
                <td class="text-center" style="width: 50%">{{trans('application.sender')}}</td>
                <td class="text-center" style="width: 50%">{{trans('application.customer')}}</td>
            </tr>
            <tr>
                <td class="text-center" style="width: 50%">({{ $department }})</td>
                <td class="text-center" style="width: 50%">({{trans('application.receiver')}})</td>
            </tr>
            <tr>
                <td class="text-center" style="height: 100px"></td>
                <td class="text-center" style="height: 100px"></td>
            </tr>
            <tr>
                <td class="text-center" style="width: 50%">{{trans('application.clear_name')}} : {{ $name }}</td>
                <td class="text-center" style="width: 50%">{{trans('application.clear_name')}} : {{ $rcv_name }} </td>
            </tr>
            <tr>
                <td class="text-center" style="width: 50%">{{trans('application.position')}} : {{ $position }}</td>
                <td class="text-center" style="width: 50%">{{trans('application.position')}} : {{ $rcv_position }}</td>
            </tr>
        </table>
    </div>
</div>
<style>
    body {font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;overflow-x: hidden;overflow-y: auto;font-size: 13px;}
    .amount_due {font-size: 16px;font-weight: 500;}
    .invoice_title{color: #2e3e4e;font-weight: bold;}
    .text-right{text-align: right;}
    .text-center{text-align: center;}
    .col-sm-12{width: 100%;}
    .col-sm-6{width: 50%;float: left;}
    table {border-spacing: 0;border-collapse: collapse;}
    .table {width: 100%;max-width: 100%;margin-bottom: 20px;}
    .item_table_header>th{padding: 8px;line-height: 1.42857143;vertical-align: top;}
    .table>tr>td, .table>tr>th{padding: 8px;line-height: 1.42857143;vertical-align: top;}
    hr.separator{border-color:  #2e3e4e;margin-top: 10px;margin-bottom: 10px;}
    tbody#items>tr>td{border: 3px solid #fff !important;vertical-align: middle;padding: 8px;}
    #items{background-color: #f1f1f1;}
    .form-group {margin-bottom: 1rem;}
    .from_address{width: 330px;margin-bottom:1rem;float: left;}
    .to_address{width: 330px;float: right;}
    .capitalize{text-transform: uppercase}
</style>