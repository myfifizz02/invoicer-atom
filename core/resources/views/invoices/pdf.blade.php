<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head><body>
<div class="container">
    <div style="width:150px;height:150px;float:left;">
        @if($invoice->pdf_logo != '')
            <img src="{{ $invoice->pdf_logo }}" alt="logo" width="80%"/>
        @endif
    </div>
    <div style="width:300px;height:150px;float:left;">
        @if($settings)
            <h1 style="margin: 0"><strong>{{ $settings->name }}</strong></h1>
            {{ $settings->address1 ? $settings->address1.',' : '' }} {{ $settings->state ? $settings->state : '' }}<br/>
            {{ $settings->city ? $settings->city.',' : '' }} {{ $settings->postal_code ? $settings->postal_code.','  : ''  }}<br/>
            {{ $settings->country }}<br/>
            @if($settings->phone != '')
                {{trans('application.phone')}}: {{ $settings->phone }}<br/>
            @endif
            @if($settings->email != '')
                {{trans('application.email')}}: {{ $settings->email }}.
            @endif
        @endif
    </div>
    <div style="clear: both"></div>
    <div class="text-right" style="width:300px;float:right;text-transform: uppercase;">
        <div class="text-right"> <h2 style="margin: 0;font-weight: bold;"> {{trans('application.invoice')}}
        @if($invoiceSettings && $invoiceSettings->show_status)
            <span style="{{ $invoice->status == 2 ? 'color:green' : 'color:red' }}">
                ({{ statuses()[$invoice->status]['label']  }})
            </span>
        @endif
        </h2></div>
    </div>
    <div style="clear: both"></div>
    <div class="panel-body" style="font-size: 16px;font-weight: bold;padding: 0; margin-top: 25px">
        <table style="width: 100%">
            <tr>
                <td class="text-left" style="width: 150px">{{trans('application.date')}}</td>
                <td style="width: 20px">:</td>
                <td class="text-left" style="color: red">{{ format_date($invoice->date) }}</td>
            </tr>
            <tr>
                <td class="text-left">{{trans('application.reference')}}</td>
                <td>:</td>
                <td class="text-left" style="color: red"> {{ $invoice->number }}</td>
            </tr>
            @if($invoice->due_date != '0000-00-00')
            <tr>
                <td class="text-left">{{trans('application.due_date')}}</td>
                <td>:</td>
                <td class="text-left" style="color: red"> {{ format_date($invoice->due_date) }}</td>
            </tr>
            @endif
            @if($settings && $settings->gst != '')
            <tr>
                <td class="text-left">{{trans('application.gst_number')}}</td>
                <td>:</td>
                <td class="text-left" style="color: red"> {{ $settings ? $settings->gst : '' }}</td>
            </tr>
            @endif
        </table>
    </div>
    <div class="col-md-12">
        <div style="color: red;font-weight: bold;margin: 25px 0">
            <div style="color: red;font-weight: bold">{{trans('application.customer')}} </div>
            {{ $invoice->client->name }}<br>
            {{ $invoice->client->address1 ? $invoice->client->address1.',' : '' }} {{ $invoice->client->state ? $invoice->client->state : '' }}<br/>
            {{ $invoice->client->city ? $invoice->client->city.',' : '' }} {{ $invoice->client->postal_code ? $invoice->client->postal_code.','  : ''  }}<br/>
            {{ $invoice->client->country }}<br/>
            @if($invoice->client->phone != '')
                {{trans('application.phone')}}: {{ $invoice->client->phone }}<br/>
            @endif
            @if($invoice->client->email != '')
                {{trans('application.email')}}: {{ $invoice->client->email }}<br>
            @endif
            @if($invoice->client->npwp != '')
                {{trans('application.tax_number')}}: {{ $invoice->client->npwp }}
            @endif
        </div>
    </div>
    <div style="clear: both"></div>
    <div class="col-md-12">
        <table class="table">
            <tr style="margin-bottom:5px;font-weight: bold;color: black">
                <th style="width:50%" align="left">{{trans('application.product')}}</th>
                <th style="width:10%" class="text-center">{{trans('application.quantity')}}</th>
                <th style="width:15%" class="text-right">{{trans('application.price')}}</th>
                <th style="width:10%" class="text-center">{{trans('application.tax')}}</th>
                <th style="width:15%" class="text-right">{{trans('application.total')}}</th>
            </tr>
            @foreach($invoice->items->sortBy('item_order') as $item)
                <tr style="background-color: white;color: red;margin-bottom: 15px">
                    <td align="left" style="padding: 8px 0"><b>{!! $item->item_name !!}</b><br/>{!! htmlspecialchars_decode(nl2br(e($item->item_description)),ENT_QUOTES) !!}</td>
                    <td class="text-center"><b>{{ $item->quantity }}</b></td>
                    <td class="text-right"><b>{{ format_amount($item->price) }}</b></td>
                    <td class="text-center"><b>{{ $item->tax ? $item->tax->value.'%' : '-' }}</b></td>
                    <td class="text-right"><b>{{ format_amount($invoice->totals[$item->uuid]['itemTotal']) }}</b></td>
                </tr>
            @endforeach
        </table>
    </div>
    <div class="col-md-12">
        <div class="col-md-6" style="padding: 7% 25% 0 10%;width: 30%; text-transform: uppercase">
        </div>
        <table class="table">
            <tr>
                <th style="width:75%" class="text-right">{{trans('application.subtotal')}}</th>
                <td class="text-right">
                    <span id="subTotal">{{ format_amount($invoice->totals['subTotal']) }}</span>
                </td>
            </tr>
            <tr>
                <th class="text-right">{{trans('application.tax')}}</th>
                <td class="text-right">
                    <span id="taxTotal">{{ format_amount($invoice->totals['taxTotal']) }}</span>
                </td>
            </tr>
            @if($invoice->totals['discount'] > 0)
                <tr>
                    <th class="text-right">{{trans('application.discount')}}</th>
                    <td class="text-right">
                        <span id="taxTotal">{{format_amount($invoice->totals['discount']) }}</span>
                    </td>
                </tr>
            @endif
            <tr class="amount_due">
                <th class="text-right">{{trans('application.total')}}</th>
                <td class="text-right">
                    <span id="grandTotal" style="color: red">{{ format_amount($invoice->totals['grandTotal']) }}</span>
                </td>
            </tr>
            @if($invoice->totals['amountDue'] > 0)
            <tr>
                <th class="text-right">{{trans('application.paid')}}</th>
                <td class="text-right">
                    <span id="grandTotal">{{ format_amount($invoice->totals['paidFormatted']) }}</span>
                </td>
            </tr>
            <tr>
                <th class="text-right">{{trans('application.amount_due')}}</th>
                <td class="text-right">
                    <span id="grandTotal">{{ format_amount($invoice->totals['amountDue']) }}</span>
                </td>
            </tr>
            @endif
        </table>
    </div>
    <div class="col-md-12">
        @if($invoice->notes)
            <h4 class="invoice_title">{{trans('application.notes')}}</h4><hr class="separator"/>
            {!! htmlspecialchars_decode($invoice->notes,ENT_QUOTES) !!} <br/>
        @endif
        @if($invoice->terms)
            <h4 class="invoice_title">{{trans('application.terms')}}</h4><hr class="separator"/>
            {!! htmlspecialchars_decode($invoice->terms,ENT_QUOTES) !!}
        @endif
    </div>
</div>
</body></html>
<style>
    body {font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;overflow-x: hidden;overflow-y: auto;font-size: 13px;}
    .amount_due {font-size: 16px;font-weight: 500;}
    .invoice_title{color: #2e3e4e;font-weight: bold;}
    .text-right{text-align: right;}
    .text-center{text-align: center;}
    .col-sm-12{width: 100%;}
    .col-sm-6{width: 50%;float: left;}
    table {border-spacing: 0;border-collapse: collapse;}
    .table {width: 100%;max-width: 100%;margin-bottom: 20px;}
    .item_table_header>th{padding: 8px;line-height: 1.42857143;vertical-align: top;}
    .table>tr>td, .table>tr>th{padding: 8px;line-height: 1.42857143;vertical-align: top;}
    hr.separator{border-color:  #2e3e4e;margin-top: 10px;margin-bottom: 10px;}
    tbody#items>tr>td{border: 3px solid #fff !important;vertical-align: middle;padding: 8px;}
    #items{background-color: #f1f1f1;}
    .form-group {margin-bottom: 1rem;}
    .from_address{width: 330px;margin-bottom:1rem;float: left;}
    .to_address{width: 330px;float: right;}
    .capitalize{text-transform: uppercase}
    .invoice_status_cancelled {font-size : 20px;text-align : center;color: #cc0000;border: 1px solid #cc0000;}
    .invoice_status_paid {font-size : 25px;text-align : center;color: #82b440;border: 1px solid #82b440;}
</style>
