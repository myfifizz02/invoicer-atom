@extends('modal')
@section('content')
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 class="modal-title">{{ trans('application.bast_desc') }}</h5>
            </div>
            {!! Form::open(['route' => ['bast_pdf', $uuid, 'BAST'], 'id' => 'printfrm', 'method' => 'get']) !!}
                <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <label>{{trans('application.sender')}}</label>
                        <div class="form-group">
                            {!! Form::label('name', trans('application.clear_name').'*') !!}
                            {!! Form::text('name', $admin_name, ['class' => 'form-control input-sm', 'required']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('position', trans('application.position').'*') !!}
                            {!! Form::text('position', null, ['class' => 'form-control input-sm', 'required']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('department', trans('application.department').'*') !!}
                            {!! Form::text('department', 'Gudang', ['class' => 'form-control input-sm', 'required']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label>{{trans('application.receiver')}}</label>
                        <div class="form-group">
                            {!! Form::label('rcv_name', trans('application.clear_name').' ('.trans('application.optional').')') !!}
                            {!! Form::text('rcv_name', $client_name, ['class' => 'form-control input-sm']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('rcv_position', trans('application.position').' ('.trans('application.optional').')') !!}
                            {!! Form::text('rcv_position', null, ['class' => 'form-control input-sm']) !!}
                        </div>
                    </div>
                </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="printbtn" data-rel="tooltip" data-placement="top" title="{{ trans('application.print') }}" class="btn btn-xs btn-success"><i class="fa fa-print"></i> {{ trans("application.print") }}</button>
                    <button type="button" data-rel="tooltip" data-placement="top" title="{{ trans('application.close') }}" data-dismiss="modal" class="btn btn-xs btn-danger"> <i class="fa fa-times"></i> {{ trans("application.close") }}</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var inputs = {
                name: localStorage.getItem('sender_name'),
                position: localStorage.getItem('sender_position'),
                department: localStorage.getItem('sender_department'),
                rcv_name: localStorage.getItem('receiver_name'),
                rcv_position: localStorage.getItem('receiver_position')
            }
            console.log(inputs);
            for (k in inputs) {
                if (inputs.hasOwnProperty(k)) {
                    if (inputs[k] != null && inputs[k].length > 0) {
                        $('#' + k).val(inputs[k]);
                    }
                }
            }
        });

        $('#printbtn').click(function (e) {
            e.preventDefault();
            localStorage.setItem('sender_name', $('#name').val());
            localStorage.setItem('sender_position', $('#position').val());
            localStorage.setItem('sender_department', $('#department').val());
            localStorage.setItem('receiver_name', $('#rcv_name').val());
            localStorage.setItem('receiver_position', $('#rcv_position').val());
            $('#printfrm').submit();
        })
    </script>
@endsection