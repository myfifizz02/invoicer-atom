<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UuidModel;
use Automattic\WooCommerce\Client;
use Automattic\WooCommerce\HttpClient\HttpClientException;

class Product extends Model {
    use UuidModel;
    public $incrementing = false;
    protected $primaryKey = 'uuid';
    protected $fillable =  ['name', 'code', 'category_id', 'price', 'description','image', 'unit', 'wc_prd_id'];

    public static function boot() {
        parent::boot();
        static::deleting(function($product) {
            if($product->image != ''){
                \File::delete(config('app.images_path').'uploads/product_images/'.$product->image);
            }
        });
    }

    public function category(){
        return $this->belongsTo(ProductCategory::class, 'category_id');
    }
    public function scopeOrdered($query){
        return $query->orderBy('created_at', 'desc')->get();
    }

    static function createFrontend($wcData, $data)
    {
        try {
            $woocommerce = new Client(config('app.shop_url'), config('app.shop_consumer_key'), config('app.shop_consumer_secret'), [
                'verify_ssl' => config('app.debug') ? false : true,
                'version' => config('app.shop_api_ver')
            ]);
            $woocommerce->post('products', $wcData);
            $response = $woocommerce->http->getResponse();
            $resBody = json_decode($response->getBody(), true);
            if (isset($resBody['id'])) {
                return $resBody['id'];
            } else {
                if (isset($data['image'])) {
                    \File::delete(config('app.images_path').'uploads/product_images/'.$data['image']);
                }
                \Log::error('WC API POST Error: ' . print_r( $resBody, true ));
                throw new \Exception("Gagal Upload ke Frontend", 1);
            }
        } catch(HttpClientException $e) {
            if (isset($data['image'])) {
                \File::delete(config('app.images_path').'uploads/product_images/'.$data['image']);
            }
            \Log::error('Exception WC API: ' . print_r( $e->getRequest(), true ));
            \Log::error('Exception WC API: ' . print_r( $e->getMessage(), true ));
            throw new \Exception("Gagal Upload ke Frontend", 1);
        }
    }

    static function updateFrontend($id, $data)
    {
        try {
            $woocommerce = new Client(config('app.shop_url'), config('app.shop_consumer_key'), config('app.shop_consumer_secret'), [
                'verify_ssl' => config('app.debug') ? false : true,
                'version' => config('app.shop_api_ver')
            ]);
            $woocommerce->put("products/$id", $data);
            $response = $woocommerce->http->getResponse();
            $resBody = json_decode($response->getBody(), true);
            if (isset($resBody['id'])) {
                return 1;
            } else {
                \File::delete(config('app.images_path').'uploads/product_images/'.$data['image']);
                \Log::error('WC API POST Error: ' . print_r( $resBody, true ));
                return 0;
                // throw new \Exception("Gagal Update ke Frontend", 1);
            }
        } catch(HttpClientException $e) {
            \File::delete(config('app.images_path').'uploads/product_images/'.$data['image']);
            \Log::error('Exception WC API: ' . print_r( $e->getMessage(), true ));
            return 0;
            // throw new \Exception("Gagal Update ke Frontend", 1);
        }
    }

    static function destroyFrontend($id)
    {
        try {
            $woocommerce = new Client(config('app.shop_url'), config('app.shop_consumer_key'), config('app.shop_consumer_secret'), [
                'verify_ssl' => config('app.debug') ? false : true,
                'version' => config('app.shop_api_ver')
            ]);
            $woocommerce->delete("products/$id", ['force' => true]);
            $response = $woocommerce->http->getResponse();
            $resBody = json_decode($response->getBody(), true);
            if (isset($resBody['id'])) {
                return 1;
            } else {
                \Log::error('WC API POST Error: ' . print_r( $resBody, true ));
                return 0;
                // throw new \Exception("Gagal Update ke Frontend", 1);
            }
        } catch(HttpClientException $e) {
            \Log::error('Exception WC API: ' . print_r( $e->getMessage(), true ));
            return 0;
            // throw new \Exception("Gagal Update ke Frontend", 1);
        }
    }
}
