<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UuidModel;
use Automattic\WooCommerce\Client;
use Automattic\WooCommerce\HttpClient\HttpClientException;

class ProductCategory extends Model
{
    use UuidModel;
    public $incrementing = false;
    protected $fillable = ['name', 'wc_cat_id'];
    protected $primaryKey = 'uuid';

    public function products(){
        return $this->hasMany(Product::class);
    }

    static function createFrontend($data)
    {
        try {
            $woocommerce = new Client(config('app.shop_url'), config('app.shop_consumer_key'), config('app.shop_consumer_secret'), [
                'verify_ssl' => config('app.debug') ? false : true,
                'version' => config('app.shop_api_ver')
            ]);
            $woocommerce->post('products/categories', ['name' => $data['name']]);
            $response = $woocommerce->http->getResponse();
            $resBody = json_decode($response->getBody(), true);
            if (isset($resBody['id'])) {
                $data['wc_cat_id'] = $resBody['id'];
                return $data;
            } else {
                \Log::error('WC API POST Error: ' . print_r( $resBody, true ));
                throw new \Exception("Gagal Upload ke Frontend", 1);
            }
        } catch(HttpClientException $e) {
        	$res = json_decode($e->getResponse()->getBody(), true);
        	if (isset($res['code']) && $res['code'] == 'term_exists') {
                $data['wc_cat_id'] = $res['data']['resource_id'];
                return $data;        		
        	} else {
	            \Log::error('Exception WC API: ' . print_r( $e->getMessage(), true ));
	            throw new \Exception("Gagal Upload ke Frontend", 1);
        	}
        }
    }

    static function updateFrontend($id, $data)
    {
    	try {
	    	$woocommerce = new Client(config('app.shop_url'), config('app.shop_consumer_key'), config('app.shop_consumer_secret'), [
                'verify_ssl' => config('app.debug') ? false : true,
                'version' => config('app.shop_api_ver')
            ]);
            $woocommerce->put("products/categories/$id", ['name' => $data['name']]);
            $response = $woocommerce->http->getResponse();
            $resBody = json_decode($response->getBody(), true);
            if (isset($resBody['id'])) {
                return 1;
            } else {
                \Log::error('WC API POST Error: ' . print_r( $resBody, true ));
                return 0;
                // throw new \Exception("Gagal Update ke Frontend", 1);
            }
        } catch(HttpClientException $e) {
            \Log::error('Exception WC API: ' . print_r( $e->getMessage(), true ));
            return 0;
            // throw new \Exception("Gagal Update ke Frontend", 1);
        }
    }

    static function destroyFrontend($id)
    {
    	try {
	    	$woocommerce = new Client(config('app.shop_url'), config('app.shop_consumer_key'), config('app.shop_consumer_secret'), [
                'verify_ssl' => config('app.debug') ? false : true,
                'version' => config('app.shop_api_ver')
            ]);
            $woocommerce->delete("products/categories/$id", ['force' => true]);
            $response = $woocommerce->http->getResponse();
            $resBody = json_decode($response->getBody(), true);
            if (isset($resBody['id'])) {
                return 1;
            } else {
                \Log::error('WC API POST Error: ' . print_r( $resBody, true ));
                return 0;
                // throw new \Exception("Gagal Update ke Frontend", 1);
            }
        } catch(HttpClientException $e) {
            \Log::error('Exception WC API: ' . print_r( $e->getMessage(), true ));
            return 0;
            // throw new \Exception("Gagal Update ke Frontend", 1);
        }
    }
}
