<?php namespace App\Http\Controllers\ClientArea;

use App\Http\Requests\EstimateFormRequest;
use App\Invoicer\Repositories\Contracts\EstimateInterface as Estimate;
use App\Invoicer\Repositories\Contracts\EstimateItemInterface as EstimateItem;
use App\Invoicer\Repositories\Contracts\EstimateSettingInterface as EstimateSetting;
use App\Invoicer\Repositories\Contracts\ProductInterface as Product;
use App\Invoicer\Repositories\Contracts\TaxSettingInterface as Tax;
use App\Invoicer\Repositories\Contracts\ClientInterface as Client;
use App\Invoicer\Repositories\Contracts\CurrencyInterface as Currency;
use App\Invoicer\Repositories\Contracts\SettingInterface as Setting;
use App\Invoicer\Repositories\Contracts\NumberSettingInterface as Number;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\Facades\DataTables;

class EstimatesController extends Controller {
    protected $product,$tax,$client,$currency,$estimate,$estimateItem,$setting,$logged_user,$estimateSetting;
    public function __construct(Product $product,Tax $tax, Client $client, Currency $currency, Estimate $estimate, EstimateItem $estimateItem, Setting $setting,EstimateSetting $estimateSetting, Number $number){
        $this->product = $product;
        $this->client = $client;
        $this->currency = $currency;
        $this->tax = $tax;
        $this->estimate = $estimate;
        $this->estimateItem = $estimateItem;
        $this->setting = $setting;
        $this->estimateSetting = $estimateSetting;
        $this->number = $number;
        $this->middleware(function ($request, $next) {
            $this->logged_user = auth()->guard('user')->user()->uuid;
            return $next($request);
        });
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        if (Request::ajax()) {
            $model = $this->estimate->model();
            $estimates = $model::with('client')->where('client_id',$this->logged_user)->select('client_id','estimate_no','estimate_date','uuid','currency')->ordered();
            return DataTables::of($estimates)
                ->addColumn('amount', function($data){
                    $totals = $this->estimate->estimateTotals($data->uuid);
                    return '<span style="display:inline-block">'.$data->currency.'</span> <span style="display:inline-block"> '.format_amount($totals['grandTotal']).'</span>';
                })->addColumn('action', '
                     <a href="{{ route(\'estimatepdf\',$uuid) }}" target="_blank" data-rel="tooltip" data-placement="top" title="{{trans(\'application.print\')}}" class="btn btn-xs btn-primary"><i class="fa fa-print"></i></a>
                     ')
                ->rawColumns(['action','amount'])
                ->make(true);
        }else {
            return view('clientarea.estimates.index');
        }
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $settings     = $this->estimateSetting->first();
        $start        = $settings ? $settings->start_number : 0;
        $estimate_num = $this->number->prefix('estimate_number', $this->estimate->generateEstimateNum($start));
        $products     = $this->product->productSelect();
        return view('clientarea.estimates.create', compact('products', 'taxes', 'currencies', 'clients', 'estimate_num','settings'));
    }
    /**
     * Store a newly created resource in storage.
     * @param EstimateFormRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function store(EstimateFormRequest $request)
    {
        $settings     = $this->estimateSetting->first();
        $start        = $settings ? $settings->start_number : 0;
        $estimate_num = $this->number->prefix('estimate_number', $this->estimate->generateEstimateNum($start));
        $estimateData = array(
            'client_id'     => $this->logged_user,
            'estimate_no'   => $estimate_num,
            'estimate_date' => date('Y-m-d'),
            'notes'         => $request->get('notes'),
            'terms'         => $request->get('terms'),
            'currency'      => 'IDR'
        );
        $estimate = $this->estimate->create($estimateData);
        if($estimate){
            $items = json_decode($request->get('items'));
            foreach($items as $item_order=>$item){
                $itemsData = array(
                    'estimate_id'           => $estimate->uuid,
                    'item_name'             => $item->item_name,
                    'item_description'      => $item->item_description,
                    'quantity'              => $item->quantity,
                    'price'                 => $item->price,
                    'unit'                 => $item->unit,
                    'item_order'            => $item_order+1
                );
                $this->estimateItem->create($itemsData);
            }

            $settings     = $this->estimateSetting->first();
            if($settings){
                $start = $settings->start_number+1;
                $this->estimateSetting->updateById($settings->uuid, array('start_number'=>$start));
            }
            return Response::json(array('success' => true,'redirectTo'=>route('cestimates.show', $estimate->uuid), 'msg' => trans('application.record_created')), 200);
        }
        return Response::json(array('success' => false, 'msg' => trans('application.record_creation_failed')), 400);
    }
    /**
     * Display the specified resource.
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($uuid)
	{
        $estimate = $this->estimate->with('items')->getById($uuid);
        if($estimate){
            $settings = $this->setting->first();
            $estimate_settings = $this->estimateSetting->first();
            $estimate->totals = $this->estimate->estimateTotals($uuid);
            return view('clientarea.estimates.show', compact('estimate', 'settings','estimate_settings'));
        }
        return Redirect::route('cestimates');
	}
    /**
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function estimatePdf($uuid){
        $estimate = $this->estimate->with('items')->getById($uuid);
        if($estimate){
            $settings = $this->setting->first();
            $estimate_settings = $this->estimateSetting->first();
            $estimate->totals = $this->estimate->estimateTotals($uuid);
            $estimate->estimate_logo = $estimate_settings && $estimate_settings->logo ? $_SERVER['DOCUMENT_ROOT'].'/'.config('app.images_path').$estimate_settings->logo : '';
            if ($estimate_settings && $estimate_settings->logo && !file_exists($estimate->estimate_logo)) {
                $estimate->estimate_logo = $estimate_settings && $estimate_settings->logo ? $_SERVER['DOCUMENT_ROOT'].env('DOC_ROOT_DIR') . '/' .config('app.images_path').$estimate_settings->logo : '';
            }
            $pdf = \PDF::loadView('clientarea.estimates.pdf', compact('settings', 'estimate','estimate_settings'));
            return $pdf->stream('estimate_'.$estimate->estimate_no.'_'.date('Y-m-d').'.pdf');
        }
        return Redirect::route('cestimates');
    }
}
